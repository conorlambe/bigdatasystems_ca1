//Question 1
1. db.movies.insert({title:"Fight Club", writer:"Chuck Palahniuk", year: 1999, actors:["Brad Pitt","Edward Norton"]})
2. db.movies.insert({title:"Pulp Fiction", writer:"Quentin Tarantino", year: 1994, actors:["John Travolta","Uma Thurman"]})
3. db.movies.insert({title:"Inglorious Basterds", writer:"Quentin Tarantino", year: 2009, actors:["Brad Piit","Diane Kruger","Eli Roth"]})
4. db.movies.insert({title:"The Hobbit: An Unexpected Journey", writer:"J.R.R. Tolkein", year: 2012, franchise: "The Hobbit"})
5. db.movies.insert({title:"The Hobbit: The Desolation of Smaug", writer:"J.R.R. Tolkein", year: 2013, franchise: "The Hobbit"})
6. db.movies.insert({title:"The Hobbit: The Battle of the Five Armies", writer:"J.R.R. Tolkein", year: 2012, franchise: "The Hobbit", synopsis: "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."})
7. db.movies.insert({title:"Pee Wee Herman's Big Adventure"})
8. db.movies.insert({title:"Avatar"})

//Question 2
1. db.movies.find().pretty()
2. db.movies.find({"writer": "Quentin Tarantino"})
3. db.movies.find({"actors": "Brad Pitt"})
4. db.movies.find({"franchise": "The Hobbit"})
5. db.movies.find({"year": {$gt:1990}, "year":{$lt:2000}})
6. db.movies.find({"year": {$lt:2000}, "year":{$gt:2010}})

//Question 3
1. db.movies.update({title:"The Hobbit: An Unexpected Journey"},{$set:{"synopsis":"A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it, from the dragon Smaug."}})
2. db.movies.update({title:"The Hobbit: The Desolation of Smaug"},{$set:{"synopsis":"The dwarves, along wiht Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring"}})
3. db.movies.update({title:"Pulp Fiction"},{$push:{"actors":"Samuel L. Jackson"}})

//Question 4
1. db.movies.find({synopsis: /Bilbo/})
2. db.movies.find({synopsis: /Gandalf/})
3. db.movies.find({$and: [{synopsis: /Bilbo/}, {synopsis: {$not: /Gandalf/}}]})
4. db.movies.find({$or: [{synopsis: /dwarves/}, {synopsis: /hobbit/}]})
5. db.movies.find({$and: [{synopsis: /gold/}, {synopsis: /dragon/}]})

//Question 5
1. db.movies.remove({title: "Pee Wee Herman's Big Adventure"})
2. db.movies.remove({title: "Avatar"})

//Question 6
1. db.users.insert({username: "GoodGuyGreg", first_name: "Good Guy", last_name: "Greg"})
2. db.users.insert({username: "ScumbagSteve", full_name: {first: "Scumbag", last: "Steve"}}) 
3. db.posts.insert({username: "GoodGuyGreg", title: "Passes out at party", body: "Wakes up early and cleans house"})
4. db.posts.insert({username: "GoodGuyGreg", title: "Steals your identity", body: "Raises your credit score"})
5. db.posts.insert({username: "GoodGuyGreg", title: "Reports a bug in your code", body: "Sends you a Pull Request"})
6. db.posts.insert({username: "ScumbagSteve", title: "Borrows soemthing", body: "Sells it"})
7. db.posts.insert({username: "ScumbagSteve", title: "Borrows everything", body: "The end"})
8. db.posts.insert({username: "ScumbagSteve", title: "Forks your repo on github", body: "Sets to private"})
9. db.comments.insert({username: "GoodGuyGreg", comment: "Hope you got a good deal!", post: [ObjectId("582a4782e1b69ad3cb6bb7a8")]})
10.db.comments.insert({username: "GoodGuyGreg", comment: "What's mine is yours!", post: [ObjectId("582a4797e1b69ad3cb6bb7a9")]})
11.db.comments.insert({username: "GoodGuyGreg", comment: "Don't violate the licensing agreement!", post:[ObjectId("582a47bbe1b69ad3cb6bb7aa")]})
12.db.comments.insert({username: "ScumbagSteve", comment: "It still isn't clean", post: [ObjectId("582a46d9e1b69ad3cb6bb7a5")]})
13.db.comments.insert({username: "ScumbagSteve", comment: "Denied your PR cause I found a hack", post:[ObjectId("582a4751e1b69ad3cb6bb7a7")]})

//Question 7
1. db.users.find().pretty()
2. db.posts.find().pretty()  
3. db.posts.find({username: "GoodGuyGreg"})
4. db.posts.find({username: "ScumbagSteve"})
5. db.comments.find().pretty()
6. db.comments.find({username: "GoodGuyGreg"})
7. db.comments.find({username: "ScumbagSteve"})
8. db.comments.find({post: [ObjectId("582a4751e1b69ad3cb6bb7a7")]})